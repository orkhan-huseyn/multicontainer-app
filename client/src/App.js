import React, { useEffect, useState } from "react";
import axios from "axios";

const App = () => {
  const [username, setUsername] = useState("");
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      const { data } = await axios.get("http://localhost:8080/users");
      setUsers(data);
    };
    fetchUsers();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    await axios.post("http://localhost:8080/users", { username });
    setUsers([...users, username]);
  };

  return (
    <div className="container">
      <form className="mb-3" onSubmit={handleSubmit}>
        <div className="mb-3">
          <label for="username" className="form-label">
            Username
          </label>
          <input
            type="text"
            id="username"
            className="form-control"
            aria-describedby="usernameHelp"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <div id="usernameHelp" className="form-text">
            We'll never share your username with anyone else.
          </div>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>

      <ul className="list-group">
        {users.map((user) => (
          <li key={user.username} className="list-group-item">
            {user.username}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default App;
