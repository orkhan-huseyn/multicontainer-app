const express = require("express");
const cors = require("cors");
const { Pool } = require("pg");

const PORT = 8080;
const app = express();

const pool = new Pool({
  user: process.env.PGUSER,
  host: process.env.PGHOST,
  database: process.env.PGDATABASE,
  password: process.env.PGPASSWORD,
  port: process.env.PGPORT,
});

app.use(express.json());
app.use(cors());

pool
  .query(
    `CREATE TABLE IF NOT EXISTS users (
      id serial PRIMARY KEY,
      username VARCHAR (50) UNIQUE NOT NULL
    )`
  )
  .catch((err) => console.log(err));

app.get("/", (req, res) => {
  res.send("Hello, World!");
});

app.get("/users", (req, res) => {
  pool.query("SELECT * FROM users").then((result) => {
    res.send(result.rows);
  });
});

app.post("/users", (req, res) => {
  const query = {
    text: "INSERT INTO users(username) VALUES($1)",
    values: [req.body.username],
  };
  pool.query(query).then((result) => {
    res.send(result.rows[0]);
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
